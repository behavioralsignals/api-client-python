# coding: utf-8

"""
    Oliver API

    Oliver API service  # noqa: E501

    OpenAPI spec version: 3.11.1
    Contact: api@behavioralsignals.com
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from __future__ import absolute_import

import unittest

import behavioral_signals_swagger_client_3
from behavioral_signals_swagger_client_3.models.speaker_behavioral_events_positivity import SpeakerBehavioralEventsPositivity  # noqa: E501
from behavioral_signals_swagger_client_3.rest import ApiException


class TestSpeakerBehavioralEventsPositivity(unittest.TestCase):
    """SpeakerBehavioralEventsPositivity unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testSpeakerBehavioralEventsPositivity(self):
        """Test SpeakerBehavioralEventsPositivity"""
        # FIXME: construct object with mandatory attributes with example values
        # model = behavioral_signals_swagger_client_3.models.speaker_behavioral_events_positivity.SpeakerBehavioralEventsPositivity()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
