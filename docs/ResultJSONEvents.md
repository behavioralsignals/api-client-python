# ResultJSONEvents

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**behavioral** | [**ResultJSONEventsBehavioral**](ResultJSONEventsBehavioral.md) |  | [optional] 
**callflow** | [**ResultJSONEventsCallflow**](ResultJSONEventsCallflow.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


