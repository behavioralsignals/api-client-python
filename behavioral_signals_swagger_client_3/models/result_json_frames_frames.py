# coding: utf-8

"""
    Oliver API

    Oliver API service  # noqa: E501

    OpenAPI spec version: 3.11.1
    Contact: api@behavioralsignals.com
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


import pprint
import re  # noqa: F401

import six


class ResultJSONFramesFrames(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'no': 'int',
        'st': 'float',
        'et': 'float',
        'intensity': 'ResultJSONFramesIntensity',
        'aced': 'str',
        'speakers': 'list[ResultJSONFramesSpeakers]'
    }

    attribute_map = {
        'no': 'no',
        'st': 'st',
        'et': 'et',
        'intensity': 'intensity',
        'aced': 'aced',
        'speakers': 'speakers'
    }

    def __init__(self, no=None, st=None, et=None, intensity=None, aced=None, speakers=None):  # noqa: E501
        """ResultJSONFramesFrames - a model defined in Swagger"""  # noqa: E501

        self._no = None
        self._st = None
        self._et = None
        self._intensity = None
        self._aced = None
        self._speakers = None
        self.discriminator = None

        self.no = no
        self.st = st
        self.et = et
        if intensity is not None:
            self.intensity = intensity
        if aced is not None:
            self.aced = aced
        if speakers is not None:
            self.speakers = speakers

    @property
    def no(self):
        """Gets the no of this ResultJSONFramesFrames.  # noqa: E501

        The current frame counter  # noqa: E501

        :return: The no of this ResultJSONFramesFrames.  # noqa: E501
        :rtype: int
        """
        return self._no

    @no.setter
    def no(self, no):
        """Sets the no of this ResultJSONFramesFrames.

        The current frame counter  # noqa: E501

        :param no: The no of this ResultJSONFramesFrames.  # noqa: E501
        :type: int
        """
        if no is None:
            raise ValueError("Invalid value for `no`, must not be `None`")  # noqa: E501

        self._no = no

    @property
    def st(self):
        """Gets the st of this ResultJSONFramesFrames.  # noqa: E501

        The start time of the frame in the call  # noqa: E501

        :return: The st of this ResultJSONFramesFrames.  # noqa: E501
        :rtype: float
        """
        return self._st

    @st.setter
    def st(self, st):
        """Sets the st of this ResultJSONFramesFrames.

        The start time of the frame in the call  # noqa: E501

        :param st: The st of this ResultJSONFramesFrames.  # noqa: E501
        :type: float
        """
        if st is None:
            raise ValueError("Invalid value for `st`, must not be `None`")  # noqa: E501

        self._st = st

    @property
    def et(self):
        """Gets the et of this ResultJSONFramesFrames.  # noqa: E501

        The end time of the frame in the call  # noqa: E501

        :return: The et of this ResultJSONFramesFrames.  # noqa: E501
        :rtype: float
        """
        return self._et

    @et.setter
    def et(self, et):
        """Sets the et of this ResultJSONFramesFrames.

        The end time of the frame in the call  # noqa: E501

        :param et: The et of this ResultJSONFramesFrames.  # noqa: E501
        :type: float
        """
        if et is None:
            raise ValueError("Invalid value for `et`, must not be `None`")  # noqa: E501

        self._et = et

    @property
    def intensity(self):
        """Gets the intensity of this ResultJSONFramesFrames.  # noqa: E501


        :return: The intensity of this ResultJSONFramesFrames.  # noqa: E501
        :rtype: ResultJSONFramesIntensity
        """
        return self._intensity

    @intensity.setter
    def intensity(self, intensity):
        """Sets the intensity of this ResultJSONFramesFrames.


        :param intensity: The intensity of this ResultJSONFramesFrames.  # noqa: E501
        :type: ResultJSONFramesIntensity
        """

        self._intensity = intensity

    @property
    def aced(self):
        """Gets the aced of this ResultJSONFramesFrames.  # noqa: E501

        Identification of speech  # noqa: E501

        :return: The aced of this ResultJSONFramesFrames.  # noqa: E501
        :rtype: str
        """
        return self._aced

    @aced.setter
    def aced(self, aced):
        """Sets the aced of this ResultJSONFramesFrames.

        Identification of speech  # noqa: E501

        :param aced: The aced of this ResultJSONFramesFrames.  # noqa: E501
        :type: str
        """

        self._aced = aced

    @property
    def speakers(self):
        """Gets the speakers of this ResultJSONFramesFrames.  # noqa: E501

        Frame results per speaker  # noqa: E501

        :return: The speakers of this ResultJSONFramesFrames.  # noqa: E501
        :rtype: list[ResultJSONFramesSpeakers]
        """
        return self._speakers

    @speakers.setter
    def speakers(self, speakers):
        """Sets the speakers of this ResultJSONFramesFrames.

        Frame results per speaker  # noqa: E501

        :param speakers: The speakers of this ResultJSONFramesFrames.  # noqa: E501
        :type: list[ResultJSONFramesSpeakers]
        """

        self._speakers = speakers

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(ResultJSONFramesFrames, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, ResultJSONFramesFrames):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
