# coding: utf-8

"""
    Oliver API

    Oliver API service  # noqa: E501

    OpenAPI spec version: 3.11.1
    Contact: api@behavioralsignals.com
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


import pprint
import re  # noqa: F401

import six


class ResultJSONKpiTrends(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'customer_positivity': 'float'
    }

    attribute_map = {
        'customer_positivity': 'customer_positivity'
    }

    def __init__(self, customer_positivity=None):  # noqa: E501
        """ResultJSONKpiTrends - a model defined in Swagger"""  # noqa: E501

        self._customer_positivity = None
        self.discriminator = None

        if customer_positivity is not None:
            self.customer_positivity = customer_positivity

    @property
    def customer_positivity(self):
        """Gets the customer_positivity of this ResultJSONKpiTrends.  # noqa: E501

        Customer resolution KPI  # noqa: E501

        :return: The customer_positivity of this ResultJSONKpiTrends.  # noqa: E501
        :rtype: float
        """
        return self._customer_positivity

    @customer_positivity.setter
    def customer_positivity(self, customer_positivity):
        """Sets the customer_positivity of this ResultJSONKpiTrends.

        Customer resolution KPI  # noqa: E501

        :param customer_positivity: The customer_positivity of this ResultJSONKpiTrends.  # noqa: E501
        :type: float
        """

        self._customer_positivity = customer_positivity

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(ResultJSONKpiTrends, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, ResultJSONKpiTrends):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
