# ResultJSON

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**basic** | [**ResultJSONBasic**](ResultJSONBasic.md) |  | 
**core** | [**ResultJSONCore**](ResultJSONCore.md) |  | 
**kpi** | [**ResultJSONKpi**](ResultJSONKpi.md) |  | 
**events** | [**ResultJSONEvents**](ResultJSONEvents.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


