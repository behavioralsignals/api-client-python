# SpeakerBehavioralEvents

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**emotion** | [**list[SpeakerBehavioralEventsEmotion]**](SpeakerBehavioralEventsEmotion.md) | the array containing all events related to discrete emotions | [optional] 
**strength** | [**list[SpeakerBehavioralEventsStrength]**](SpeakerBehavioralEventsStrength.md) | the array containing all events related to activation | [optional] 
**positivity** | [**list[SpeakerBehavioralEventsPositivity]**](SpeakerBehavioralEventsPositivity.md) | the array containing all events related to positivity | [optional] 
**engagement** | [**list[SpeakerBehavioralEventsEngagement]**](SpeakerBehavioralEventsEngagement.md) | the array containing all events related to engagement | [optional] 
**politeness** | [**list[SpeakerBehavioralEventsPoliteness]**](SpeakerBehavioralEventsPoliteness.md) | the array containing all events related to politeness | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


