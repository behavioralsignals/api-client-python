# ResultJSONFeatures

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**features** | [**list[ResultJSONFeaturesFeatures]**](ResultJSONFeaturesFeatures.md) | Contains feature results | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


