# ResultJSONFrames

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**frames** | [**list[ResultJSONFramesFrames]**](ResultJSONFramesFrames.md) | Contains frame results | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


