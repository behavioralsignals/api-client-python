# ProcessUpdatedStatus

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pid** | **int** | Unique ID for the processing job | 
**status** | **int** | Shows the status of the job ufter update | 
**message** | **str** | A message with update detail | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


