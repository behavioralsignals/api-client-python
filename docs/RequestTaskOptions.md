# RequestTaskOptions

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**replacement** | **str** | The string to use for replacing pii data | [optional] 
**activate** | **bool** | Whether to activate the task operation | [optional] 
**data** | [**RequestTaskOptionsData**](RequestTaskOptionsData.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


