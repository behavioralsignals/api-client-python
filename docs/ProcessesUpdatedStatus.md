# ProcessesUpdatedStatus

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**action** | **str** |  | [optional] 
**total** | **int** |  | [optional] 
**updated** | **int** |  | [optional] 
**failed** | [**list[ProcessUpdatedStatus]**](ProcessUpdatedStatus.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


