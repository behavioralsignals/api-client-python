# ResultJSONEventsBehavioral

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**agent** | [**SpeakerBehavioralEvents**](SpeakerBehavioralEvents.md) |  | [optional] 
**customer** | [**SpeakerBehavioralEvents**](SpeakerBehavioralEvents.md) |  | [optional] 
**speaker1** | [**SpeakerBehavioralEvents**](SpeakerBehavioralEvents.md) |  | [optional] 
**speaker2** | [**SpeakerBehavioralEvents**](SpeakerBehavioralEvents.md) |  | [optional] 
**speaker3** | [**SpeakerBehavioralEvents**](SpeakerBehavioralEvents.md) |  | [optional] 
**speaker4** | [**SpeakerBehavioralEvents**](SpeakerBehavioralEvents.md) |  | [optional] 
**speaker5** | [**SpeakerBehavioralEvents**](SpeakerBehavioralEvents.md) |  | [optional] 
**speaker6** | [**SpeakerBehavioralEvents**](SpeakerBehavioralEvents.md) |  | [optional] 
**speaker7** | [**SpeakerBehavioralEvents**](SpeakerBehavioralEvents.md) |  | [optional] 
**speaker8** | [**SpeakerBehavioralEvents**](SpeakerBehavioralEvents.md) |  | [optional] 
**speaker9** | [**SpeakerBehavioralEvents**](SpeakerBehavioralEvents.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


