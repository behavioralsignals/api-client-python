# ResultJSONBasicLangSpanish

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**agent** | **float** | Agent language score for spanish | [optional] 
**customer** | **float** | Customer language score for spanish | [optional] 
**speaker1** | **float** | Speaker language score for spanish | [optional] 
**speaker2** | **float** | Speaker language score for spanish | [optional] 
**speaker3** | **float** | Speaker language score for spanish | [optional] 
**speaker4** | **float** | Speaker language score for spanish | [optional] 
**speaker5** | **float** | Speaker language score for spanish | [optional] 
**speaker6** | **float** | Speaker language score for spanish | [optional] 
**speaker7** | **float** | Speaker language score for spanish | [optional] 
**speaker8** | **float** | Speaker language score for spanish | [optional] 
**speaker9** | **float** | Speaker language score for spanish | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


