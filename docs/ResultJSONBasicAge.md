# ResultJSONBasicAge

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**agent** | **int** | Agent age group index: 1&#x3D;adult, 2&#x3D;elderly[, 3&#x3D;child] | [optional] 
**customer** | **int** | Customer age group index: 1&#x3D;adult, 2&#x3D;elderly, 3&#x3D;child | [optional] 
**speaker1** | **int** | Customer age group index: 1&#x3D;adult, 2&#x3D;elderly, 3&#x3D;child | [optional] 
**speaker2** | **int** | Customer age group index: 1&#x3D;adult, 2&#x3D;elderly, 3&#x3D;child | [optional] 
**speaker3** | **int** | Customer age group index: 1&#x3D;adult, 2&#x3D;elderly, 3&#x3D;child | [optional] 
**speaker4** | **int** | Customer age group index: 1&#x3D;adult, 2&#x3D;elderly, 3&#x3D;child | [optional] 
**speaker5** | **int** | Customer age group index: 1&#x3D;adult, 2&#x3D;elderly, 3&#x3D;child | [optional] 
**speaker6** | **int** | Customer age group index: 1&#x3D;adult, 2&#x3D;elderly, 3&#x3D;child | [optional] 
**speaker7** | **int** | Customer age group index: 1&#x3D;adult, 2&#x3D;elderly, 3&#x3D;child | [optional] 
**speaker8** | **int** | Customer age group index: 1&#x3D;adult, 2&#x3D;elderly, 3&#x3D;child | [optional] 
**speaker9** | **int** | Customer age group index: 1&#x3D;adult, 2&#x3D;elderly, 3&#x3D;child | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


