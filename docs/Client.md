# Client

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cid** | **int** | Client&#39;s ID in our system | [optional] 
**name** | **str** | Client&#39;s Username | [optional] 
**email** | **str** | Client&#39;s email address | [optional] 
**tag** | **list[str]** | Tag of customer | [optional] 
**storedata** | **int** | Indicates if the client has enabled the data storage option. Can take 3 values: 0 &#x3D; no storage, 1 &#x3D; storage based on a per process plan, 2 &#x3D; storage for all incoming job data | [optional] 
**datastore** | **str** | The data store that audio data will be kept. Usually it is an S3 bucket. Will be concatenated with process information to create the uri for each individual data file. | [optional] 
**models_id** | **str** | Clients associated models groups | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


