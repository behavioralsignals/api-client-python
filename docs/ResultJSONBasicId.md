# ResultJSONBasicId

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**agent** | **int** | The id for the agent | [optional] 
**customer** | **int** | The id for the customer | [optional] 
**speaker1** | **int** | The id for the speaker | [optional] 
**speaker2** | **int** | The id for the speaker | [optional] 
**speaker3** | **int** | The id for the speaker | [optional] 
**speaker4** | **int** | The id for the speaker | [optional] 
**speaker5** | **int** | The id for the speaker | [optional] 
**speaker6** | **int** | The id for the speaker | [optional] 
**speaker7** | **int** | The id for the speaker | [optional] 
**speaker8** | **int** | The id for the speaker | [optional] 
**speaker9** | **int** | The id for the speaker | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


