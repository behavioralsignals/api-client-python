# ResultJSONFramesFrames

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**no** | **int** | The current frame counter | 
**st** | **float** | The start time of the frame in the call | 
**et** | **float** | The end time of the frame in the call | 
**intensity** | [**ResultJSONFramesIntensity**](ResultJSONFramesIntensity.md) |  | [optional] 
**aced** | **str** | Identification of speech | [optional] 
**speakers** | [**list[ResultJSONFramesSpeakers]**](ResultJSONFramesSpeakers.md) | Frame results per speaker | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


