# ResultJSONBasicLangEnglish

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**agent** | **float** | Agent language score for english | [optional] 
**customer** | **float** | Customer language score for english | [optional] 
**speaker1** | **float** | Speaker language score for english | [optional] 
**speaker2** | **float** | Speaker language score for english | [optional] 
**speaker3** | **float** | Speaker language score for english | [optional] 
**speaker4** | **float** | Speaker language score for english | [optional] 
**speaker5** | **float** | Speaker language score for english | [optional] 
**speaker6** | **float** | Speaker language score for english | [optional] 
**speaker7** | **float** | Speaker language score for english | [optional] 
**speaker8** | **float** | Speaker language score for english | [optional] 
**speaker9** | **float** | Speaker language score for english | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


