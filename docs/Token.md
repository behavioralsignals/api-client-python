# Token

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**token** | **str** | The token hash string | 
**role** | **str** | The role can be user/administrator | 
**cid** | **int** | the client id that will be associated with this token | 
**status** | **int** | The status can be -1/0/1 expired/disabled/active | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


