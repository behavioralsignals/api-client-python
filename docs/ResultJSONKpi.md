# ResultJSONKpi

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**escalation** | **bool** | Escalation during the call, e.g., negative emotions are on the rise | [optional] 
**resolution** | **bool** | Resolution during the call, e.g., negative emotions are falling | [optional] 
**agentperfscore** | **float** | Agent performance score, normalized between 0 (bad)  and 1 (excellent) | [optional] 
**satisfaction** | **float** | Customer satisfaction, normalized between 0 and 1 (100% satisfied) | [optional] 
**propensity** | **float** | Propensity to buy/donate, normalized between 0 (no sale) and 1 (sale) | [optional] 
**success** | **float** | Call success, normalized between 0 (fail) and 1 (success) | [optional] 
**negativity** | **float** | Probability that the call contains negative emotions, 0 corresponds to no negative emotions whereas 1 only to negative emotions | [optional] 
**agent_empathy** | **float** | Probability that the agent exhibits empathy between 0 (non empathetic) and 1 (empathetic) | [optional] 
**customer_satisfaction** | **float** | Probability that the customer had a positive experience between 0 (dissatisfied) and 1 (satisfied) | [optional] 
**trends** | [**ResultJSONKpiTrends**](ResultJSONKpiTrends.md) |  | [optional] 
**compliance** | [**ResultJSONKpiCompliance**](ResultJSONKpiCompliance.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


