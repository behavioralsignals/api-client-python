# ProcessStats

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cid** | **int** | Client ID that requested the processing | [optional] 
**pid** | **int** | Unique ID for the processing job | 
**status** | **int** | Shows the processing state of the job. Status is 0: pending, 1: processing, 2: completed, -1:failed, -2 aborted | [optional] 
**total** | **float** | The time from the moment inserted in the queue till reaching final state | [optional] 
**pending** | **float** | Total time the audio was on the queue | [optional] 
**processing** | **float** | Total time the audio was in processing state | [optional] 
**retrying** | **float** | Total time the audio was in failed state | [optional] 
**retries** | **int** | Number of retries before transiting to a final state | [optional] 
**extended_timers** | **list[float]** | Extended Timers | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


