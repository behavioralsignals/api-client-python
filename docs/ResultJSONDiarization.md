# ResultJSONDiarization

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**diarization** | [**list[ResultJSONDiarizationDiarization]**](ResultJSONDiarizationDiarization.md) | Contains diarization segments | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


