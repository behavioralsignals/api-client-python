# RequestTaskOptionsData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**speaker1** | [**SpeakerDiarizationRanges**](SpeakerDiarizationRanges.md) |  | [optional] 
**speaker2** | [**SpeakerDiarizationRanges**](SpeakerDiarizationRanges.md) |  | [optional] 
**speaker3** | [**SpeakerDiarizationRanges**](SpeakerDiarizationRanges.md) |  | [optional] 
**speaker4** | [**SpeakerDiarizationRanges**](SpeakerDiarizationRanges.md) |  | [optional] 
**speaker5** | [**SpeakerDiarizationRanges**](SpeakerDiarizationRanges.md) |  | [optional] 
**speaker6** | [**SpeakerDiarizationRanges**](SpeakerDiarizationRanges.md) |  | [optional] 
**speaker7** | [**SpeakerDiarizationRanges**](SpeakerDiarizationRanges.md) |  | [optional] 
**speaker8** | [**SpeakerDiarizationRanges**](SpeakerDiarizationRanges.md) |  | [optional] 
**speaker9** | [**SpeakerDiarizationRanges**](SpeakerDiarizationRanges.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


