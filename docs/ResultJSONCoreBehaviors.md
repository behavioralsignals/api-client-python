# ResultJSONCoreBehaviors

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **str** | Classification task name, e.g. arousal | [optional] 
**value** | [**list[ResultJSONCoreValue1]**](ResultJSONCoreValue1.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


