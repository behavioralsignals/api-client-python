# ProcessesUpdate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**action** | **str** | Defines the final state of the processes ids, e.g abort | [default to 'abort']
**pids** | **object** | An object containing a list of process ids | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


