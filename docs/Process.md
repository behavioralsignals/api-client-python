# Process

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pid** | **int** | Unique ID for the processing job | 
**cid** | **int** | Client ID that requested the processing | [optional] 
**name** | **str** | Label of the processing job (Client defined) | [optional] 
**status** | **int** | Shows the processing state of the job. Status is 0: pending, 1: processing, 2: completed, -1:failed, -2 aborted | [optional] 
**statusmsg** | **str** | Reason for success or failure | [optional] 
**duration** | **float** | duration of the audio signal (in sec) | [optional] 
**_datetime** | **datetime** | date and time the request for processing was inserted into the system | [optional] 
**uri** | **str** | where to find original audio data in file store | [optional] 
**internal** | **str** | for internal system use | [optional] 
**customer_id** | **str** | Customer ID | [optional] 
**customer_ind** | **str** | Customer Industry Index | [optional] 
**agent_id** | **str** | Agent ID | [optional] 
**campaign_id** | **str** | Campaign ID | [optional] 
**agent_team** | **str** | Agent&#39;s team ID | [optional] 
**calltype** | **str** | The type of call | [optional] 
**calltime** | **datetime** | Call time | [optional] 
**timezone** | **int** | Timezone of call | [optional] 
**calldirection** | **int** | Who initiated the call 1&#x3D;incoming, 2&#x3D;outgoing | 
**channels** | **int** | Number of channels for audio signal 1&#x3D;single OR  2&#x3D;two-channel | 
**ani** | **str** | ANI information | [optional] 
**meta** | **str** | A JSON string containing call metadata not included already as parameters | [optional] 
**predictionmode** | **str** | Sets the prediction mode on process, whether to get predictions based on audio or transcription or both of them. | [optional] 
**tasks** | **str** | A JSON string containing call tasks not included already as parameters | [optional] 
**tag** | **list[str]** | Client defined tagging of processing job | [optional] 
**ports** | **list[int]** | Internal ports for live mode request | [optional] 
**mode** | **int** | The processing job mode of operation 1&#x3D;URL, 2&#x3D;Audio file, 3&#x3D;Websocket, 4&#x3D;Video | [optional] 
**storedata** | **int** | Boolean value to show if this process saves audio data in the storage. 0&#x3D;no and 1&#x3D;yes. Look the uri property for the location of the file in the cloud. | [optional] 
**source** | **str** | Original url for post url method, filename for post formdata method, null for live method | [optional] 
**ip** | **str** | Remote ip accessing the API; special case is the web demo post | [optional] 
**mimetype** | **str** | The audio format of the incoming audio, which must be included in the supported list | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


