# ResultJSONBasicLang

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**english** | [**ResultJSONBasicLangEnglish**](ResultJSONBasicLangEnglish.md) |  | [optional] 
**spanish** | [**ResultJSONBasicLangSpanish**](ResultJSONBasicLangSpanish.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


