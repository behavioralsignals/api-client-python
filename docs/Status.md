# Status

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**response** | **int** | Code response of service status | 
**detail** | **str** | Human readable status | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


