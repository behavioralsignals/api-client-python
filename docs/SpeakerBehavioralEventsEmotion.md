# SpeakerBehavioralEventsEmotion

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**st** | **float** | start time in seconds | [optional] 
**et** | **float** | end time in seconds | [optional] 
**label** | **str** | event label: happy/angry/sad/frustrated | [optional] 
**confidence** | **float** | confidence score, between 0 and 1 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


