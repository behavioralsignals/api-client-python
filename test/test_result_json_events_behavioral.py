# coding: utf-8

"""
    Oliver API

    Oliver API service  # noqa: E501

    OpenAPI spec version: 3.11.1
    Contact: api@behavioralsignals.com
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from __future__ import absolute_import

import unittest

import behavioral_signals_swagger_client_3
from behavioral_signals_swagger_client_3.models.result_json_events_behavioral import ResultJSONEventsBehavioral  # noqa: E501
from behavioral_signals_swagger_client_3.rest import ApiException


class TestResultJSONEventsBehavioral(unittest.TestCase):
    """ResultJSONEventsBehavioral unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testResultJSONEventsBehavioral(self):
        """Test ResultJSONEventsBehavioral"""
        # FIXME: construct object with mandatory attributes with example values
        # model = behavioral_signals_swagger_client_3.models.result_json_events_behavioral.ResultJSONEventsBehavioral()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
