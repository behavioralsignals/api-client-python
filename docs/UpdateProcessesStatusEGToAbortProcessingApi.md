# behavioral_signals_swagger_client_3.UpdateProcessesStatusEGToAbortProcessingApi

All URIs are relative to *https://api.behavioralsignals.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**update_processes**](UpdateProcessesStatusEGToAbortProcessingApi.md#update_processes) | **PUT** /client/{cid}/process | 


# **update_processes**
> ProcessesUpdatedStatus update_processes(cid, processes_update=processes_update)



Update status for a list of processes belonging to a client

### Example
```python
from __future__ import print_function
import time
import behavioral_signals_swagger_client_3
from behavioral_signals_swagger_client_3.rest import ApiException
from pprint import pprint

# Configure API key authorization: token
configuration = behavioral_signals_swagger_client_3.Configuration()
configuration.api_key['X-Auth-Token'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-Auth-Token'] = 'Bearer'

# create an instance of the API class
api_instance = behavioral_signals_swagger_client_3.UpdateProcessesStatusEGToAbortProcessingApi(behavioral_signals_swagger_client_3.ApiClient(configuration))
cid = 789 # int | id of the requested client
processes_update = behavioral_signals_swagger_client_3.ProcessesUpdate() # ProcessesUpdate | List of process ids to update status for (optional)

try:
    api_response = api_instance.update_processes(cid, processes_update=processes_update)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UpdateProcessesStatusEGToAbortProcessingApi->update_processes: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cid** | **int**| id of the requested client | 
 **processes_update** | [**ProcessesUpdate**](ProcessesUpdate.md)| List of process ids to update status for | [optional] 

### Return type

[**ProcessesUpdatedStatus**](ProcessesUpdatedStatus.md)

### Authorization

[token](../README.md#token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

