# ResultJSONBasicUtterances

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**agent** | **int** | Number of agent utterances | [optional] 
**customer** | **int** | Number of customer utterances | [optional] 
**speaker1** | **int** | Number of speaker utterances | [optional] 
**speaker2** | **int** | Number of speaker utterances | [optional] 
**speaker3** | **int** | Number of speaker utterances | [optional] 
**speaker4** | **int** | Number of speaker utterances | [optional] 
**speaker5** | **int** | Number of speaker utterances | [optional] 
**speaker6** | **int** | Number of speaker utterances | [optional] 
**speaker7** | **int** | Number of speaker utterances | [optional] 
**speaker8** | **int** | Number of speaker utterances | [optional] 
**speaker9** | **int** | Number of speaker utterances | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


