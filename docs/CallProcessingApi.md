# behavioral_signals_swagger_client_3.CallProcessingApi

All URIs are relative to *https://api.behavioralsignals.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_all_processes**](CallProcessingApi.md#get_all_processes) | **GET** /client/{cid}/process | 


# **get_all_processes**
> ArrayOfProcesses get_all_processes(cid)



Returns all processes of a client

### Example
```python
from __future__ import print_function
import time
import behavioral_signals_swagger_client_3
from behavioral_signals_swagger_client_3.rest import ApiException
from pprint import pprint

# Configure API key authorization: token
configuration = behavioral_signals_swagger_client_3.Configuration()
configuration.api_key['X-Auth-Token'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-Auth-Token'] = 'Bearer'

# create an instance of the API class
api_instance = behavioral_signals_swagger_client_3.CallProcessingApi(behavioral_signals_swagger_client_3.ApiClient(configuration))
cid = 789 # int | ID of client giving request

try:
    api_response = api_instance.get_all_processes(cid)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CallProcessingApi->get_all_processes: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cid** | **int**| ID of client giving request | 

### Return type

[**ArrayOfProcesses**](ArrayOfProcesses.md)

### Authorization

[token](../README.md#token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

