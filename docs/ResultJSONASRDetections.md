# ResultJSONASRDetections

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**detected_key** | **str** | Type of the key used for detection | [optional] 
**occurrences** | [**list[ResultJSONASROccurrences]**](ResultJSONASROccurrences.md) | Timings of the detected occurrences on ASR result | [optional] 
**replacement** | **str** | Text to use for the replacement of detected occurrences | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


