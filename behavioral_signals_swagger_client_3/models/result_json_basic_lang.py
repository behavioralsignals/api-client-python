# coding: utf-8

"""
    Oliver API

    Oliver API service  # noqa: E501

    OpenAPI spec version: 3.11.1
    Contact: api@behavioralsignals.com
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


import pprint
import re  # noqa: F401

import six


class ResultJSONBasicLang(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'english': 'ResultJSONBasicLangEnglish',
        'spanish': 'ResultJSONBasicLangSpanish'
    }

    attribute_map = {
        'english': 'english',
        'spanish': 'spanish'
    }

    def __init__(self, english=None, spanish=None):  # noqa: E501
        """ResultJSONBasicLang - a model defined in Swagger"""  # noqa: E501

        self._english = None
        self._spanish = None
        self.discriminator = None

        if english is not None:
            self.english = english
        if spanish is not None:
            self.spanish = spanish

    @property
    def english(self):
        """Gets the english of this ResultJSONBasicLang.  # noqa: E501


        :return: The english of this ResultJSONBasicLang.  # noqa: E501
        :rtype: ResultJSONBasicLangEnglish
        """
        return self._english

    @english.setter
    def english(self, english):
        """Sets the english of this ResultJSONBasicLang.


        :param english: The english of this ResultJSONBasicLang.  # noqa: E501
        :type: ResultJSONBasicLangEnglish
        """

        self._english = english

    @property
    def spanish(self):
        """Gets the spanish of this ResultJSONBasicLang.  # noqa: E501


        :return: The spanish of this ResultJSONBasicLang.  # noqa: E501
        :rtype: ResultJSONBasicLangSpanish
        """
        return self._spanish

    @spanish.setter
    def spanish(self, spanish):
        """Sets the spanish of this ResultJSONBasicLang.


        :param spanish: The spanish of this ResultJSONBasicLang.  # noqa: E501
        :type: ResultJSONBasicLangSpanish
        """

        self._spanish = spanish

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(ResultJSONBasicLang, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, ResultJSONBasicLang):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
