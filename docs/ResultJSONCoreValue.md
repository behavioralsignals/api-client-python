# ResultJSONCoreValue

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**agent** | **float** | Aggregated value in the 0, 1 range | [optional] 
**customer** | **float** | Aggregated value in the 0, 1 range | [optional] 
**speaker1** | **float** | Aggregated value in the 0, 1 range | [optional] 
**speaker2** | **float** | Aggregated value in the 0, 1 range | [optional] 
**speaker3** | **float** | Aggregated value in the 0, 1 range | [optional] 
**speaker4** | **float** | Aggregated value in the 0, 1 range | [optional] 
**speaker5** | **float** | Aggregated value in the 0, 1 range | [optional] 
**speaker6** | **float** | Aggregated value in the 0, 1 range | [optional] 
**speaker7** | **float** | Aggregated value in the 0, 1 range | [optional] 
**speaker8** | **float** | Aggregated value in the 0, 1 range | [optional] 
**speaker9** | **float** | Aggregated value in the 0, 1 range | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


