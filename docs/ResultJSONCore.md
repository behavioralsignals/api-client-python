# ResultJSONCore

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**behaviors** | [**list[ResultJSONCoreBehaviors]**](ResultJSONCoreBehaviors.md) | Contains all aggregated behavior predictions for all classification tasks | [optional] 
**diarization** | [**ResultJSONCoreDiarization**](ResultJSONCoreDiarization.md) |  | [optional] 
**strength** | [**ResultJSONCoreStrength**](ResultJSONCoreStrength.md) |  | [optional] 
**hesitation** | [**ResultJSONCoreHesitation**](ResultJSONCoreHesitation.md) |  | [optional] 
**positivity** | [**ResultJSONCorePositivity**](ResultJSONCorePositivity.md) |  | [optional] 
**emotions** | [**ResultJSONCoreEmotions**](ResultJSONCoreEmotions.md) |  | [optional] 
**engagement** | [**ResultJSONCoreEngagement**](ResultJSONCoreEngagement.md) |  | [optional] 
**politeness** | [**ResultJSONCorePoliteness**](ResultJSONCorePoliteness.md) |  | [optional] 
**empathy** | [**ResultJSONCoreEmpathy**](ResultJSONCoreEmpathy.md) |  | [optional] 
**soundquality** | [**ResultJSONCoreSoundquality**](ResultJSONCoreSoundquality.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


