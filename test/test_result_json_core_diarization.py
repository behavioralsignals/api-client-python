# coding: utf-8

"""
    Oliver API

    Oliver API service  # noqa: E501

    OpenAPI spec version: 3.11.1
    Contact: api@behavioralsignals.com
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from __future__ import absolute_import

import unittest

import behavioral_signals_swagger_client_3
from behavioral_signals_swagger_client_3.models.result_json_core_diarization import ResultJSONCoreDiarization  # noqa: E501
from behavioral_signals_swagger_client_3.rest import ApiException


class TestResultJSONCoreDiarization(unittest.TestCase):
    """ResultJSONCoreDiarization unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testResultJSONCoreDiarization(self):
        """Test ResultJSONCoreDiarization"""
        # FIXME: construct object with mandatory attributes with example values
        # model = behavioral_signals_swagger_client_3.models.result_json_core_diarization.ResultJSONCoreDiarization()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
