# ResultJSONFramesStrength

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**framelevel** | **float** | Result of current frame | [optional] 
**uptonow** | **float** | Decision up to current frame | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


