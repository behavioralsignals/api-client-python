# ResultJSONCoreEmotionsFrustration

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**agent** | **float** |  | [optional] 
**customer** | **float** |  | [optional] 
**speaker1** | **float** |  | [optional] 
**speaker2** | **float** |  | [optional] 
**speaker3** | **float** |  | [optional] 
**speaker4** | **float** |  | [optional] 
**speaker5** | **float** |  | [optional] 
**speaker6** | **float** |  | [optional] 
**speaker7** | **float** |  | [optional] 
**speaker8** | **float** |  | [optional] 
**speaker9** | **float** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


