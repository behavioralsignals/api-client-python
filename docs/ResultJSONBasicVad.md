# ResultJSONBasicVad

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**agent** | **float** | Agent talk time  in seconds | [optional] 
**customer** | **float** | Customer talk time in seconds | [optional] 
**overlap** | **float** | overlap talk in seconds | [optional] 
**silence** | **float** | silence in seconds | [optional] 
**speaker1** | **float** | speaker talk time in seconds | [optional] 
**speaker2** | **float** | speaker talk time in seconds | [optional] 
**speaker3** | **float** | speaker talk time in seconds | [optional] 
**speaker4** | **float** | speaker talk time in seconds | [optional] 
**speaker5** | **float** | speaker talk time in seconds | [optional] 
**speaker6** | **float** | speaker talk time in seconds | [optional] 
**speaker7** | **float** | speaker talk time in seconds | [optional] 
**speaker8** | **float** | speaker talk time in seconds | [optional] 
**speaker9** | **float** | speaker talk time in seconds | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


