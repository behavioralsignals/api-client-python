# ResultJSONASR

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**words** | [**list[ResultJSONASRWords]**](ResultJSONASRWords.md) | Contains transcribed word list | 
**predictions** | [**list[ResultJSONASRPredictions]**](ResultJSONASRPredictions.md) | Predictions based on associated task | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


