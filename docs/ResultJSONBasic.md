# ResultJSONBasic

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**totalduration** | **float** | Duration of call in seconds | [optional] 
**speakerchanges** | **int** | Number of speaker changes (turns) | [optional] 
**numbeepfound** | **int** | Number of beeps found in call | [optional] 
**numringfound** | **int** | Number of rings found in call | [optional] 
**id** | [**ResultJSONBasicId**](ResultJSONBasicId.md) |  | [optional] 
**vad** | [**ResultJSONBasicVad**](ResultJSONBasicVad.md) |  | [optional] 
**gender** | [**ResultJSONBasicGender**](ResultJSONBasicGender.md) |  | [optional] 
**age** | [**ResultJSONBasicAge**](ResultJSONBasicAge.md) |  | [optional] 
**lang** | [**ResultJSONBasicLang**](ResultJSONBasicLang.md) |  | [optional] 
**utterances** | [**ResultJSONBasicUtterances**](ResultJSONBasicUtterances.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


