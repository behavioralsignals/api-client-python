# ResultJSONASRWords

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**c** | **float** | Confidence | [optional] 
**st** | **float** | The start time of the word in seconds from begining | [optional] 
**et** | **float** | The end time of the word in seconds from begining | [optional] 
**p** | **int** | Index starting from zero | [optional] 
**w** | **str** | The transcribed word string | [optional] 
**m** | **str** | Flag | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


