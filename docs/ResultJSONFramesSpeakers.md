# ResultJSONFramesSpeakers

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | The speaker index | [optional] 
**vad** | [**ResultJSONFramesVad**](ResultJSONFramesVad.md) |  | [optional] 
**beep** | [**ResultJSONFramesBeep**](ResultJSONFramesBeep.md) |  | [optional] 
**ring** | [**ResultJSONFramesRing**](ResultJSONFramesRing.md) |  | [optional] 
**strength** | [**ResultJSONFramesStrength**](ResultJSONFramesStrength.md) |  | [optional] 
**snr** | [**ResultJSONFramesSnr**](ResultJSONFramesSnr.md) |  | [optional] 
**positivity** | [**ResultJSONFramesPositivity**](ResultJSONFramesPositivity.md) |  | [optional] 
**emotion** | [**ResultJSONFramesEmotion**](ResultJSONFramesEmotion.md) |  | [optional] 
**gender** | [**ResultJSONFramesGender**](ResultJSONFramesGender.md) |  | [optional] 
**language** | [**ResultJSONFramesLanguage**](ResultJSONFramesLanguage.md) |  | [optional] 
**age** | [**ResultJSONFramesAge**](ResultJSONFramesAge.md) |  | [optional] 
**engagement** | [**ResultJSONFramesEngagement**](ResultJSONFramesEngagement.md) |  | [optional] 
**activation** | [**ResultJSONFramesActivation**](ResultJSONFramesActivation.md) |  | [optional] 
**politeness** | [**ResultJSONFramesPoliteness**](ResultJSONFramesPoliteness.md) |  | [optional] 
**success** | [**ResultJSONFramesSuccess**](ResultJSONFramesSuccess.md) |  | [optional] 
**speaking_rate** | [**ResultJSONFramesSpeakingRate**](ResultJSONFramesSpeakingRate.md) |  | [optional] 
**hesitation** | [**ResultJSONFramesHesitation**](ResultJSONFramesHesitation.md) |  | [optional] 
**tone_variety** | [**ResultJSONFramesToneVariety**](ResultJSONFramesToneVariety.md) |  | [optional] 
**f0** | [**ResultJSONFramesF0**](ResultJSONFramesF0.md) |  | [optional] 
**escalation** | **int** | Escalation | [optional] 
**resolution** | **int** | Resolution | [optional] 
**empathy** | **float** | Speaker empathy score | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


