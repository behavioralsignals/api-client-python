# ResultJSONASROccurrences

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**st** | **float** | The start time of the frame in the call | [optional] 
**et** | **float** | The end time of the frame in the call | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


