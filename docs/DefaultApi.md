# behavioral_signals_swagger_client_3.DefaultApi

All URIs are relative to *https://api.behavioralsignals.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**client_auth**](DefaultApi.md#client_auth) | **GET** /auth | 
[**create_client**](DefaultApi.md#create_client) | **POST** /client | 
[**delete_client**](DefaultApi.md#delete_client) | **DELETE** /client/{cid} | 
[**get_clients**](DefaultApi.md#get_clients) | **GET** /client | 
[**get_process_info**](DefaultApi.md#get_process_info) | **GET** /client/{cid}/process/{pid} | 
[**get_process_results**](DefaultApi.md#get_process_results) | **GET** /client/{cid}/process/{pid}/result | 
[**get_process_results_asr**](DefaultApi.md#get_process_results_asr) | **GET** /client/{cid}/process/{pid}/resultasr | 
[**get_process_results_demo**](DefaultApi.md#get_process_results_demo) | **GET** /client/{cid}/process/{pid}/resultdemo | 
[**get_process_results_diarization**](DefaultApi.md#get_process_results_diarization) | **GET** /client/{cid}/process/{pid}/resultdiarization | 
[**get_process_results_features**](DefaultApi.md#get_process_results_features) | **GET** /client/{cid}/process/{pid}/resultfeatures | 
[**get_process_results_frames**](DefaultApi.md#get_process_results_frames) | **GET** /client/{cid}/process/{pid}/resultframes | 
[**get_process_stats**](DefaultApi.md#get_process_stats) | **GET** /client/{cid}/process/{pid}/stats | 
[**info_client**](DefaultApi.md#info_client) | **GET** /client/{cid} | 
[**modify_token**](DefaultApi.md#modify_token) | **PUT** /token | 
[**post_feedback**](DefaultApi.md#post_feedback) | **POST** /client/{cid}/process/feedback | 
[**put_feedback**](DefaultApi.md#put_feedback) | **PUT** /client/{cid}/process/feedback/{formCode} | 
[**send_process_audio**](DefaultApi.md#send_process_audio) | **POST** /client/{cid}/process/audio | 
[**send_process_tcp**](DefaultApi.md#send_process_tcp) | **POST** /client/{cid}/process/stream | 
[**send_process_url**](DefaultApi.md#send_process_url) | **POST** /client/{cid}/process/url | 
[**send_process_video**](DefaultApi.md#send_process_video) | **POST** /client/{cid}/process/video | 
[**service_status**](DefaultApi.md#service_status) | **GET** /status | 


# **client_auth**
> Authorized client_auth(x_auth_client)



Authenticates a client

### Example
```python
from __future__ import print_function
import time
import behavioral_signals_swagger_client_3
from behavioral_signals_swagger_client_3.rest import ApiException
from pprint import pprint

# Configure API key authorization: token
configuration = behavioral_signals_swagger_client_3.Configuration()
configuration.api_key['X-Auth-Token'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-Auth-Token'] = 'Bearer'

# create an instance of the API class
api_instance = behavioral_signals_swagger_client_3.DefaultApi(behavioral_signals_swagger_client_3.ApiClient(configuration))
x_auth_client = 789 # int | id of the requested client

try:
    api_response = api_instance.client_auth(x_auth_client)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->client_auth: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **x_auth_client** | **int**| id of the requested client | 

### Return type

[**Authorized**](Authorized.md)

### Authorization

[token](../README.md#token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_client**
> Client create_client(x_auth_client, client=client)



Create a new client

### Example
```python
from __future__ import print_function
import time
import behavioral_signals_swagger_client_3
from behavioral_signals_swagger_client_3.rest import ApiException
from pprint import pprint

# Configure API key authorization: token
configuration = behavioral_signals_swagger_client_3.Configuration()
configuration.api_key['X-Auth-Token'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-Auth-Token'] = 'Bearer'

# create an instance of the API class
api_instance = behavioral_signals_swagger_client_3.DefaultApi(behavioral_signals_swagger_client_3.ApiClient(configuration))
x_auth_client = 'x_auth_client_example' # str | The client id that is authorized to do this task
client = behavioral_signals_swagger_client_3.Client() # Client | The client to create (optional)

try:
    api_response = api_instance.create_client(x_auth_client, client=client)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->create_client: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **x_auth_client** | **str**| The client id that is authorized to do this task | 
 **client** | [**Client**](Client.md)| The client to create | [optional] 

### Return type

[**Client**](Client.md)

### Authorization

[token](../README.md#token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_client**
> GenericResponse delete_client(cid, x_auth_client)



Delete a client with given cid

### Example
```python
from __future__ import print_function
import time
import behavioral_signals_swagger_client_3
from behavioral_signals_swagger_client_3.rest import ApiException
from pprint import pprint

# Configure API key authorization: token
configuration = behavioral_signals_swagger_client_3.Configuration()
configuration.api_key['X-Auth-Token'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-Auth-Token'] = 'Bearer'

# create an instance of the API class
api_instance = behavioral_signals_swagger_client_3.DefaultApi(behavioral_signals_swagger_client_3.ApiClient(configuration))
cid = 789 # int | id of the requested client
x_auth_client = 'x_auth_client_example' # str | The client id that is authorized to do this task

try:
    api_response = api_instance.delete_client(cid, x_auth_client)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->delete_client: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cid** | **int**| id of the requested client | 
 **x_auth_client** | **str**| The client id that is authorized to do this task | 

### Return type

[**GenericResponse**](GenericResponse.md)

### Authorization

[token](../README.md#token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_clients**
> Clients get_clients(x_auth_client)



Get clients list

### Example
```python
from __future__ import print_function
import time
import behavioral_signals_swagger_client_3
from behavioral_signals_swagger_client_3.rest import ApiException
from pprint import pprint

# Configure API key authorization: token
configuration = behavioral_signals_swagger_client_3.Configuration()
configuration.api_key['X-Auth-Token'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-Auth-Token'] = 'Bearer'

# create an instance of the API class
api_instance = behavioral_signals_swagger_client_3.DefaultApi(behavioral_signals_swagger_client_3.ApiClient(configuration))
x_auth_client = 'x_auth_client_example' # str | The client id that is authorized to do this task

try:
    api_response = api_instance.get_clients(x_auth_client)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_clients: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **x_auth_client** | **str**| The client id that is authorized to do this task | 

### Return type

[**Clients**](Clients.md)

### Authorization

[token](../README.md#token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_process_info**
> Process get_process_info(cid, pid)



Returns process info for pid by client request

### Example
```python
from __future__ import print_function
import time
import behavioral_signals_swagger_client_3
from behavioral_signals_swagger_client_3.rest import ApiException
from pprint import pprint

# Configure API key authorization: token
configuration = behavioral_signals_swagger_client_3.Configuration()
configuration.api_key['X-Auth-Token'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-Auth-Token'] = 'Bearer'

# create an instance of the API class
api_instance = behavioral_signals_swagger_client_3.DefaultApi(behavioral_signals_swagger_client_3.ApiClient(configuration))
cid = 789 # int | ID of client giving request
pid = 789 # int | ID of the process request

try:
    api_response = api_instance.get_process_info(cid, pid)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_process_info: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cid** | **int**| ID of client giving request | 
 **pid** | **int**| ID of the process request | 

### Return type

[**Process**](Process.md)

### Authorization

[token](../README.md#token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_process_results**
> ResultJSON get_process_results(cid, pid)



Returns process results in JSON for pid by client request

### Example
```python
from __future__ import print_function
import time
import behavioral_signals_swagger_client_3
from behavioral_signals_swagger_client_3.rest import ApiException
from pprint import pprint

# Configure API key authorization: token
configuration = behavioral_signals_swagger_client_3.Configuration()
configuration.api_key['X-Auth-Token'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-Auth-Token'] = 'Bearer'

# create an instance of the API class
api_instance = behavioral_signals_swagger_client_3.DefaultApi(behavioral_signals_swagger_client_3.ApiClient(configuration))
cid = 789 # int | ID of client giving request
pid = 789 # int | ID of the process request

try:
    api_response = api_instance.get_process_results(cid, pid)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_process_results: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cid** | **int**| ID of client giving request | 
 **pid** | **int**| ID of the process request | 

### Return type

[**ResultJSON**](ResultJSON.md)

### Authorization

[token](../README.md#token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_process_results_asr**
> ResultJSONASR get_process_results_asr(cid, pid)



Returns speech-to-text transcription in JSON for process

### Example
```python
from __future__ import print_function
import time
import behavioral_signals_swagger_client_3
from behavioral_signals_swagger_client_3.rest import ApiException
from pprint import pprint

# Configure API key authorization: token
configuration = behavioral_signals_swagger_client_3.Configuration()
configuration.api_key['X-Auth-Token'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-Auth-Token'] = 'Bearer'

# create an instance of the API class
api_instance = behavioral_signals_swagger_client_3.DefaultApi(behavioral_signals_swagger_client_3.ApiClient(configuration))
cid = 789 # int | ID of client giving request
pid = 789 # int | ID of the process request

try:
    api_response = api_instance.get_process_results_asr(cid, pid)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_process_results_asr: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cid** | **int**| ID of client giving request | 
 **pid** | **int**| ID of the process request | 

### Return type

[**ResultJSONASR**](ResultJSONASR.md)

### Authorization

[token](../README.md#token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_process_results_demo**
> ResultJSONFrames get_process_results_demo(cid, pid)



Returns process results for demo in JSON for pid by client request

### Example
```python
from __future__ import print_function
import time
import behavioral_signals_swagger_client_3
from behavioral_signals_swagger_client_3.rest import ApiException
from pprint import pprint

# Configure API key authorization: token
configuration = behavioral_signals_swagger_client_3.Configuration()
configuration.api_key['X-Auth-Token'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-Auth-Token'] = 'Bearer'

# create an instance of the API class
api_instance = behavioral_signals_swagger_client_3.DefaultApi(behavioral_signals_swagger_client_3.ApiClient(configuration))
cid = 789 # int | ID of client giving request
pid = 789 # int | ID of the process request

try:
    api_response = api_instance.get_process_results_demo(cid, pid)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_process_results_demo: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cid** | **int**| ID of client giving request | 
 **pid** | **int**| ID of the process request | 

### Return type

[**ResultJSONFrames**](ResultJSONFrames.md)

### Authorization

[token](../README.md#token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_process_results_diarization**
> ResultJSONDiarization get_process_results_diarization(cid, pid)



Returns diarization in JSON for process

### Example
```python
from __future__ import print_function
import time
import behavioral_signals_swagger_client_3
from behavioral_signals_swagger_client_3.rest import ApiException
from pprint import pprint

# Configure API key authorization: token
configuration = behavioral_signals_swagger_client_3.Configuration()
configuration.api_key['X-Auth-Token'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-Auth-Token'] = 'Bearer'

# create an instance of the API class
api_instance = behavioral_signals_swagger_client_3.DefaultApi(behavioral_signals_swagger_client_3.ApiClient(configuration))
cid = 789 # int | ID of client giving request
pid = 789 # int | ID of the process request

try:
    api_response = api_instance.get_process_results_diarization(cid, pid)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_process_results_diarization: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cid** | **int**| ID of client giving request | 
 **pid** | **int**| ID of the process request | 

### Return type

[**ResultJSONDiarization**](ResultJSONDiarization.md)

### Authorization

[token](../README.md#token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_process_results_features**
> ResultJSONFeatures get_process_results_features(cid, pid)



Returns process features results in JSON for pid by client request

### Example
```python
from __future__ import print_function
import time
import behavioral_signals_swagger_client_3
from behavioral_signals_swagger_client_3.rest import ApiException
from pprint import pprint

# Configure API key authorization: token
configuration = behavioral_signals_swagger_client_3.Configuration()
configuration.api_key['X-Auth-Token'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-Auth-Token'] = 'Bearer'

# create an instance of the API class
api_instance = behavioral_signals_swagger_client_3.DefaultApi(behavioral_signals_swagger_client_3.ApiClient(configuration))
cid = 789 # int | ID of client giving request
pid = 789 # int | ID of the process request

try:
    api_response = api_instance.get_process_results_features(cid, pid)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_process_results_features: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cid** | **int**| ID of client giving request | 
 **pid** | **int**| ID of the process request | 

### Return type

[**ResultJSONFeatures**](ResultJSONFeatures.md)

### Authorization

[token](../README.md#token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_process_results_frames**
> ResultJSONFrames get_process_results_frames(cid, pid)



Returns process frames results in JSON for pid by client request

### Example
```python
from __future__ import print_function
import time
import behavioral_signals_swagger_client_3
from behavioral_signals_swagger_client_3.rest import ApiException
from pprint import pprint

# Configure API key authorization: token
configuration = behavioral_signals_swagger_client_3.Configuration()
configuration.api_key['X-Auth-Token'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-Auth-Token'] = 'Bearer'

# create an instance of the API class
api_instance = behavioral_signals_swagger_client_3.DefaultApi(behavioral_signals_swagger_client_3.ApiClient(configuration))
cid = 789 # int | ID of client giving request
pid = 789 # int | ID of the process request

try:
    api_response = api_instance.get_process_results_frames(cid, pid)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_process_results_frames: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cid** | **int**| ID of client giving request | 
 **pid** | **int**| ID of the process request | 

### Return type

[**ResultJSONFrames**](ResultJSONFrames.md)

### Authorization

[token](../README.md#token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_process_stats**
> ProcessStats get_process_stats(cid, pid)



Returns statistics for process on client request

### Example
```python
from __future__ import print_function
import time
import behavioral_signals_swagger_client_3
from behavioral_signals_swagger_client_3.rest import ApiException
from pprint import pprint

# Configure API key authorization: token
configuration = behavioral_signals_swagger_client_3.Configuration()
configuration.api_key['X-Auth-Token'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-Auth-Token'] = 'Bearer'

# create an instance of the API class
api_instance = behavioral_signals_swagger_client_3.DefaultApi(behavioral_signals_swagger_client_3.ApiClient(configuration))
cid = 789 # int | ID of client giving request
pid = 789 # int | ID of the process request

try:
    api_response = api_instance.get_process_stats(cid, pid)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_process_stats: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cid** | **int**| ID of client giving request | 
 **pid** | **int**| ID of the process request | 

### Return type

[**ProcessStats**](ProcessStats.md)

### Authorization

[token](../README.md#token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **info_client**
> Client info_client(cid)



Return a client's details

### Example
```python
from __future__ import print_function
import time
import behavioral_signals_swagger_client_3
from behavioral_signals_swagger_client_3.rest import ApiException
from pprint import pprint

# Configure API key authorization: token
configuration = behavioral_signals_swagger_client_3.Configuration()
configuration.api_key['X-Auth-Token'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-Auth-Token'] = 'Bearer'

# create an instance of the API class
api_instance = behavioral_signals_swagger_client_3.DefaultApi(behavioral_signals_swagger_client_3.ApiClient(configuration))
cid = 789 # int | id of the requested client

try:
    api_response = api_instance.info_client(cid)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->info_client: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cid** | **int**| id of the requested client | 

### Return type

[**Client**](Client.md)

### Authorization

[token](../README.md#token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **modify_token**
> Token modify_token(x_auth_client, token=token)



Inserts/Updates a token

### Example
```python
from __future__ import print_function
import time
import behavioral_signals_swagger_client_3
from behavioral_signals_swagger_client_3.rest import ApiException
from pprint import pprint

# Configure API key authorization: token
configuration = behavioral_signals_swagger_client_3.Configuration()
configuration.api_key['X-Auth-Token'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-Auth-Token'] = 'Bearer'

# create an instance of the API class
api_instance = behavioral_signals_swagger_client_3.DefaultApi(behavioral_signals_swagger_client_3.ApiClient(configuration))
x_auth_client = 'x_auth_client_example' # str | The client id that is authorized to do this task
token = behavioral_signals_swagger_client_3.Token() # Token | The token to add/update (optional)

try:
    api_response = api_instance.modify_token(x_auth_client, token=token)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->modify_token: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **x_auth_client** | **str**| The client id that is authorized to do this task | 
 **token** | [**Token**](Token.md)| The token to add/update | [optional] 

### Return type

[**Token**](Token.md)

### Authorization

[token](../README.md#token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_feedback**
> ResponseFeedback post_feedback(cid, in_json)



Post feedback request for a process. It returns a URL with the form to fill in.

### Example
```python
from __future__ import print_function
import time
import behavioral_signals_swagger_client_3
from behavioral_signals_swagger_client_3.rest import ApiException
from pprint import pprint

# Configure API key authorization: token
configuration = behavioral_signals_swagger_client_3.Configuration()
configuration.api_key['X-Auth-Token'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-Auth-Token'] = 'Bearer'

# create an instance of the API class
api_instance = behavioral_signals_swagger_client_3.DefaultApi(behavioral_signals_swagger_client_3.ApiClient(configuration))
cid = 789 # int | ID of client giving request
in_json = behavioral_signals_swagger_client_3.InputFeedback() # InputFeedback | The input JSON

try:
    api_response = api_instance.post_feedback(cid, in_json)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->post_feedback: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cid** | **int**| ID of client giving request | 
 **in_json** | [**InputFeedback**](InputFeedback.md)| The input JSON | 

### Return type

[**ResponseFeedback**](ResponseFeedback.md)

### Authorization

[token](../README.md#token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_feedback**
> str put_feedback(cid, form_code, in_json)



Sets the feedback JSON for a request with given formCode

### Example
```python
from __future__ import print_function
import time
import behavioral_signals_swagger_client_3
from behavioral_signals_swagger_client_3.rest import ApiException
from pprint import pprint

# Configure API key authorization: token
configuration = behavioral_signals_swagger_client_3.Configuration()
configuration.api_key['X-Auth-Token'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-Auth-Token'] = 'Bearer'

# create an instance of the API class
api_instance = behavioral_signals_swagger_client_3.DefaultApi(behavioral_signals_swagger_client_3.ApiClient(configuration))
cid = 789 # int | ID of client giving request
form_code = 'form_code_example' # str | Form code to update the feedback JSON
in_json = 'in_json_example' # str | the input JSON read as string

try:
    api_response = api_instance.put_feedback(cid, form_code, in_json)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->put_feedback: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cid** | **int**| ID of client giving request | 
 **form_code** | **str**| Form code to update the feedback JSON | 
 **in_json** | **str**| the input JSON read as string | 

### Return type

**str**

### Authorization

[token](../README.md#token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **send_process_audio**
> Process send_process_audio(cid, channels, calldirection, file, name=name, customer_id=customer_id, customer_ind=customer_ind, agent_team=agent_team, agent_id=agent_id, campaign_id=campaign_id, calltype=calltype, ani=ani, calltime=calltime, timezone=timezone, speakers=speakers, storedata=storedata, tag=tag, meta=meta, predictionmode=predictionmode, data=data)



Sends a new request with audio file upload

### Example
```python
from __future__ import print_function
import time
import behavioral_signals_swagger_client_3
from behavioral_signals_swagger_client_3.rest import ApiException
from pprint import pprint

# Configure API key authorization: token
configuration = behavioral_signals_swagger_client_3.Configuration()
configuration.api_key['X-Auth-Token'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-Auth-Token'] = 'Bearer'

# create an instance of the API class
api_instance = behavioral_signals_swagger_client_3.DefaultApi(behavioral_signals_swagger_client_3.ApiClient(configuration))
cid = 789 # int | ID of client giving request
channels = 56 # int | number of channels
calldirection = 56 # int | 1 for incoming,2 for outgoing
file = '/path/to/file.txt' # file | Audio file to process
name = 'name_example' # str | Optionally, a name for the job request (optional)
customer_id = 'customer_id_example' # str | The Customer's ID (optional)
customer_ind = 'customer_ind_example' # str | The Customer's Industry Index (optional)
agent_team = 'agent_team_example' # str | Agent’s team ID (optional)
agent_id = 'agent_id_example' # str | Agent's ID (optional)
campaign_id = 'campaign_id_example' # str | Campaign's ID (optional)
calltype = 'calltype_example' # str | The type of call: LA (live answer), AM (answering machine) (optional)
ani = 'ani_example' # str | ANI information (optional)
calltime = '2013-10-20T19:20:30+01:00' # datetime | The date and time the call took place (optional)
timezone = 56 # int | The timezone of the call as integer value (optional)
speakers = 56 # int | The number of speaker present in the audio. Set to zero for unknown (optional)
storedata = 56 # int | Request to store incoming process data on a per job basis. If the client has not enabled the datastore option or enabled for all requests (per client basis) then this value is ignored. Can take values 0=no storage, 1=yes. (optional)
tag = ['tag_example'] # list[str] | Client defined tagging of processing job (optional)
meta = 'meta_example' # str | Call metadata json with properties not covered in query parameters (optional)
predictionmode = 'full' # str | Sets the prediction mode on process, whether to get predictions based on audio or transcription or both of them. (optional) (default to full)
data = 'data_example' # str | Stringified data object conforming to RequestProcessBody (optional)

try:
    api_response = api_instance.send_process_audio(cid, channels, calldirection, file, name=name, customer_id=customer_id, customer_ind=customer_ind, agent_team=agent_team, agent_id=agent_id, campaign_id=campaign_id, calltype=calltype, ani=ani, calltime=calltime, timezone=timezone, speakers=speakers, storedata=storedata, tag=tag, meta=meta, predictionmode=predictionmode, data=data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->send_process_audio: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cid** | **int**| ID of client giving request | 
 **channels** | **int**| number of channels | 
 **calldirection** | **int**| 1 for incoming,2 for outgoing | 
 **file** | **file**| Audio file to process | 
 **name** | **str**| Optionally, a name for the job request | [optional] 
 **customer_id** | **str**| The Customer&#39;s ID | [optional] 
 **customer_ind** | **str**| The Customer&#39;s Industry Index | [optional] 
 **agent_team** | **str**| Agent’s team ID | [optional] 
 **agent_id** | **str**| Agent&#39;s ID | [optional] 
 **campaign_id** | **str**| Campaign&#39;s ID | [optional] 
 **calltype** | **str**| The type of call: LA (live answer), AM (answering machine) | [optional] 
 **ani** | **str**| ANI information | [optional] 
 **calltime** | **datetime**| The date and time the call took place | [optional] 
 **timezone** | **int**| The timezone of the call as integer value | [optional] 
 **speakers** | **int**| The number of speaker present in the audio. Set to zero for unknown | [optional] 
 **storedata** | **int**| Request to store incoming process data on a per job basis. If the client has not enabled the datastore option or enabled for all requests (per client basis) then this value is ignored. Can take values 0&#x3D;no storage, 1&#x3D;yes. | [optional] 
 **tag** | [**list[str]**](str.md)| Client defined tagging of processing job | [optional] 
 **meta** | **str**| Call metadata json with properties not covered in query parameters | [optional] 
 **predictionmode** | **str**| Sets the prediction mode on process, whether to get predictions based on audio or transcription or both of them. | [optional] [default to full]
 **data** | **str**| Stringified data object conforming to RequestProcessBody | [optional] 

### Return type

[**Process**](Process.md)

### Authorization

[token](../README.md#token)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **send_process_tcp**
> Process send_process_tcp(cid, channels, calldirection, name=name, customer_id=customer_id, customer_ind=customer_ind, agent_team=agent_team, agent_id=agent_id, campaign_id=campaign_id, calltype=calltype, ani=ani, calltime=calltime, timezone=timezone, storedata=storedata, tag=tag, meta=meta, tasks=tasks)



Sends a new request in stream mode

### Example
```python
from __future__ import print_function
import time
import behavioral_signals_swagger_client_3
from behavioral_signals_swagger_client_3.rest import ApiException
from pprint import pprint

# Configure API key authorization: token
configuration = behavioral_signals_swagger_client_3.Configuration()
configuration.api_key['X-Auth-Token'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-Auth-Token'] = 'Bearer'

# create an instance of the API class
api_instance = behavioral_signals_swagger_client_3.DefaultApi(behavioral_signals_swagger_client_3.ApiClient(configuration))
cid = 789 # int | ID of client giving request
channels = 56 # int | number of channels
calldirection = 56 # int | 1 for incoming,2 for outgoing
name = 'name_example' # str | Optionally, a name for the job request (optional)
customer_id = 'customer_id_example' # str | The Customer's ID (optional)
customer_ind = 'customer_ind_example' # str | The Customer's Industry Index (optional)
agent_team = 'agent_team_example' # str | Agent’s team ID (optional)
agent_id = 'agent_id_example' # str | Agent's ID (optional)
campaign_id = 'campaign_id_example' # str | Campaign's ID (optional)
calltype = 'calltype_example' # str | The type of call: LA (live answer), AM (answering machine) (optional)
ani = 'ani_example' # str | ANI information (optional)
calltime = '2013-10-20T19:20:30+01:00' # datetime | The date and time the call took place (optional)
timezone = 56 # int | The timezone of the call as integer value (optional)
storedata = 56 # int | Request to store incoming process data on a per job basis. If the client has not enabled the datastore option or enabled for all requests (per client basis) then this value is ignored. Can take values 0=no storage, 1=yes. (optional)
tag = ['tag_example'] # list[str] | Client defined tagging of processing job (optional)
meta = 'meta_example' # str | Call metadata json with properties not covered in query parameters (optional)
tasks = 'tasks_example' # str | Call tasks json with properties not covered in query parameters (optional)

try:
    api_response = api_instance.send_process_tcp(cid, channels, calldirection, name=name, customer_id=customer_id, customer_ind=customer_ind, agent_team=agent_team, agent_id=agent_id, campaign_id=campaign_id, calltype=calltype, ani=ani, calltime=calltime, timezone=timezone, storedata=storedata, tag=tag, meta=meta, tasks=tasks)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->send_process_tcp: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cid** | **int**| ID of client giving request | 
 **channels** | **int**| number of channels | 
 **calldirection** | **int**| 1 for incoming,2 for outgoing | 
 **name** | **str**| Optionally, a name for the job request | [optional] 
 **customer_id** | **str**| The Customer&#39;s ID | [optional] 
 **customer_ind** | **str**| The Customer&#39;s Industry Index | [optional] 
 **agent_team** | **str**| Agent’s team ID | [optional] 
 **agent_id** | **str**| Agent&#39;s ID | [optional] 
 **campaign_id** | **str**| Campaign&#39;s ID | [optional] 
 **calltype** | **str**| The type of call: LA (live answer), AM (answering machine) | [optional] 
 **ani** | **str**| ANI information | [optional] 
 **calltime** | **datetime**| The date and time the call took place | [optional] 
 **timezone** | **int**| The timezone of the call as integer value | [optional] 
 **storedata** | **int**| Request to store incoming process data on a per job basis. If the client has not enabled the datastore option or enabled for all requests (per client basis) then this value is ignored. Can take values 0&#x3D;no storage, 1&#x3D;yes. | [optional] 
 **tag** | [**list[str]**](str.md)| Client defined tagging of processing job | [optional] 
 **meta** | **str**| Call metadata json with properties not covered in query parameters | [optional] 
 **tasks** | **str**| Call tasks json with properties not covered in query parameters | [optional] 

### Return type

[**Process**](Process.md)

### Authorization

[token](../README.md#token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **send_process_url**
> Process send_process_url(cid, url, channels, calldirection, name=name, customer_id=customer_id, customer_ind=customer_ind, agent_team=agent_team, agent_id=agent_id, campaign_id=campaign_id, calltype=calltype, ani=ani, calltime=calltime, timezone=timezone, speakers=speakers, storedata=storedata, tag=tag, meta=meta, predictionmode=predictionmode, data=data)



Sends a new request with a url for data

### Example
```python
from __future__ import print_function
import time
import behavioral_signals_swagger_client_3
from behavioral_signals_swagger_client_3.rest import ApiException
from pprint import pprint

# Configure API key authorization: token
configuration = behavioral_signals_swagger_client_3.Configuration()
configuration.api_key['X-Auth-Token'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-Auth-Token'] = 'Bearer'

# create an instance of the API class
api_instance = behavioral_signals_swagger_client_3.DefaultApi(behavioral_signals_swagger_client_3.ApiClient(configuration))
cid = 789 # int | ID of client giving request
url = 'url_example' # str | Audio url to process
channels = 56 # int | number of channels
calldirection = 56 # int | 1 for incoming,2 for outgoing
name = 'name_example' # str | Optionally, a name for the job request (optional)
customer_id = 'customer_id_example' # str | The Customer's ID (optional)
customer_ind = 'customer_ind_example' # str | The Customer's Industry Index (optional)
agent_team = 'agent_team_example' # str | Agent’s team ID (optional)
agent_id = 'agent_id_example' # str | Agent's ID (optional)
campaign_id = 'campaign_id_example' # str | Campaign's ID (optional)
calltype = 'calltype_example' # str | The type of call: LA (live answer), AM (answering machine) (optional)
ani = 'ani_example' # str | ANI information (optional)
calltime = '2013-10-20T19:20:30+01:00' # datetime | The date and time the call took place (optional)
timezone = 56 # int | The timezone of the call as integer value (optional)
speakers = 56 # int | The number of speaker present in the audio. Set to zero for unknown (optional)
storedata = 56 # int | Request to store incoming process data on a per job basis. If the client has not enabled the datastore option or enabled for all requests (per client basis) then this value is ignored. Can take values 0=no storage, 1=yes. (optional)
tag = ['tag_example'] # list[str] | Client defined tagging of processing job (optional)
meta = 'meta_example' # str | Call metadata json with properties not covered in query parameters (optional)
predictionmode = 'full' # str | Sets the prediction mode on process, whether to get predictions based on audio or transcription or both of them. (optional) (default to full)
data = behavioral_signals_swagger_client_3.RequestProcessBody() # RequestProcessBody |  (optional)

try:
    api_response = api_instance.send_process_url(cid, url, channels, calldirection, name=name, customer_id=customer_id, customer_ind=customer_ind, agent_team=agent_team, agent_id=agent_id, campaign_id=campaign_id, calltype=calltype, ani=ani, calltime=calltime, timezone=timezone, speakers=speakers, storedata=storedata, tag=tag, meta=meta, predictionmode=predictionmode, data=data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->send_process_url: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cid** | **int**| ID of client giving request | 
 **url** | **str**| Audio url to process | 
 **channels** | **int**| number of channels | 
 **calldirection** | **int**| 1 for incoming,2 for outgoing | 
 **name** | **str**| Optionally, a name for the job request | [optional] 
 **customer_id** | **str**| The Customer&#39;s ID | [optional] 
 **customer_ind** | **str**| The Customer&#39;s Industry Index | [optional] 
 **agent_team** | **str**| Agent’s team ID | [optional] 
 **agent_id** | **str**| Agent&#39;s ID | [optional] 
 **campaign_id** | **str**| Campaign&#39;s ID | [optional] 
 **calltype** | **str**| The type of call: LA (live answer), AM (answering machine) | [optional] 
 **ani** | **str**| ANI information | [optional] 
 **calltime** | **datetime**| The date and time the call took place | [optional] 
 **timezone** | **int**| The timezone of the call as integer value | [optional] 
 **speakers** | **int**| The number of speaker present in the audio. Set to zero for unknown | [optional] 
 **storedata** | **int**| Request to store incoming process data on a per job basis. If the client has not enabled the datastore option or enabled for all requests (per client basis) then this value is ignored. Can take values 0&#x3D;no storage, 1&#x3D;yes. | [optional] 
 **tag** | [**list[str]**](str.md)| Client defined tagging of processing job | [optional] 
 **meta** | **str**| Call metadata json with properties not covered in query parameters | [optional] 
 **predictionmode** | **str**| Sets the prediction mode on process, whether to get predictions based on audio or transcription or both of them. | [optional] [default to full]
 **data** | [**RequestProcessBody**](RequestProcessBody.md)|  | [optional] 

### Return type

[**Process**](Process.md)

### Authorization

[token](../README.md#token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **send_process_video**
> Process send_process_video(cid, url, channels, calldirection, name=name, customer_id=customer_id, customer_ind=customer_ind, agent_team=agent_team, agent_id=agent_id, campaign_id=campaign_id, calltype=calltype, ani=ani, calltime=calltime, timezone=timezone, speakers=speakers, storedata=storedata, tag=tag, meta=meta, predictionmode=predictionmode, data=data)



Sends a new request to process a video file from url

### Example
```python
from __future__ import print_function
import time
import behavioral_signals_swagger_client_3
from behavioral_signals_swagger_client_3.rest import ApiException
from pprint import pprint

# Configure API key authorization: token
configuration = behavioral_signals_swagger_client_3.Configuration()
configuration.api_key['X-Auth-Token'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-Auth-Token'] = 'Bearer'

# create an instance of the API class
api_instance = behavioral_signals_swagger_client_3.DefaultApi(behavioral_signals_swagger_client_3.ApiClient(configuration))
cid = 789 # int | ID of client giving request
url = 'url_example' # str | Video url to process
channels = 56 # int | number of channels
calldirection = 56 # int | 1 for incoming,2 for outgoing
name = 'name_example' # str | Optionally, a name for the job request (optional)
customer_id = 'customer_id_example' # str | The Customer's ID (optional)
customer_ind = 'customer_ind_example' # str | The Customer's Industry Index (optional)
agent_team = 'agent_team_example' # str | Agent’s team ID (optional)
agent_id = 'agent_id_example' # str | Agent's ID (optional)
campaign_id = 'campaign_id_example' # str | Campaign's ID (optional)
calltype = 'calltype_example' # str | The type of call: LA (live answer), AM (answering machine) (optional)
ani = 'ani_example' # str | ANI information (optional)
calltime = '2013-10-20T19:20:30+01:00' # datetime | The date and time the call took place (optional)
timezone = 56 # int | The timezone of the call as integer value (optional)
speakers = 56 # int | The number of speaker present in the video. Set to zero for unknown (optional)
storedata = 56 # int | Request to store incoming process data on a per job basis. If the client has not enabled the datastore option or enabled for all requests (per client basis) then this value is ignored. Can take values 0=no storage, 1=yes. (optional)
tag = ['tag_example'] # list[str] | Client defined tagging of processing job (optional)
meta = 'meta_example' # str | Call metadata json with properties not covered in query parameters (optional)
predictionmode = 'full' # str | Sets the prediction mode on process, whether to get predictions based on audio or transcription or both of them. (optional) (default to full)
data = behavioral_signals_swagger_client_3.RequestProcessBody() # RequestProcessBody |  (optional)

try:
    api_response = api_instance.send_process_video(cid, url, channels, calldirection, name=name, customer_id=customer_id, customer_ind=customer_ind, agent_team=agent_team, agent_id=agent_id, campaign_id=campaign_id, calltype=calltype, ani=ani, calltime=calltime, timezone=timezone, speakers=speakers, storedata=storedata, tag=tag, meta=meta, predictionmode=predictionmode, data=data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->send_process_video: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cid** | **int**| ID of client giving request | 
 **url** | **str**| Video url to process | 
 **channels** | **int**| number of channels | 
 **calldirection** | **int**| 1 for incoming,2 for outgoing | 
 **name** | **str**| Optionally, a name for the job request | [optional] 
 **customer_id** | **str**| The Customer&#39;s ID | [optional] 
 **customer_ind** | **str**| The Customer&#39;s Industry Index | [optional] 
 **agent_team** | **str**| Agent’s team ID | [optional] 
 **agent_id** | **str**| Agent&#39;s ID | [optional] 
 **campaign_id** | **str**| Campaign&#39;s ID | [optional] 
 **calltype** | **str**| The type of call: LA (live answer), AM (answering machine) | [optional] 
 **ani** | **str**| ANI information | [optional] 
 **calltime** | **datetime**| The date and time the call took place | [optional] 
 **timezone** | **int**| The timezone of the call as integer value | [optional] 
 **speakers** | **int**| The number of speaker present in the video. Set to zero for unknown | [optional] 
 **storedata** | **int**| Request to store incoming process data on a per job basis. If the client has not enabled the datastore option or enabled for all requests (per client basis) then this value is ignored. Can take values 0&#x3D;no storage, 1&#x3D;yes. | [optional] 
 **tag** | [**list[str]**](str.md)| Client defined tagging of processing job | [optional] 
 **meta** | **str**| Call metadata json with properties not covered in query parameters | [optional] 
 **predictionmode** | **str**| Sets the prediction mode on process, whether to get predictions based on audio or transcription or both of them. | [optional] [default to full]
 **data** | [**RequestProcessBody**](RequestProcessBody.md)|  | [optional] 

### Return type

[**Process**](Process.md)

### Authorization

[token](../README.md#token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **service_status**
> Status service_status()



Return status of the service

### Example
```python
from __future__ import print_function
import time
import behavioral_signals_swagger_client_3
from behavioral_signals_swagger_client_3.rest import ApiException
from pprint import pprint

# Configure API key authorization: token
configuration = behavioral_signals_swagger_client_3.Configuration()
configuration.api_key['X-Auth-Token'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-Auth-Token'] = 'Bearer'

# create an instance of the API class
api_instance = behavioral_signals_swagger_client_3.DefaultApi(behavioral_signals_swagger_client_3.ApiClient(configuration))

try:
    api_response = api_instance.service_status()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->service_status: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**Status**](Status.md)

### Authorization

[token](../README.md#token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

