# coding: utf-8

"""
    Oliver API

    Oliver API service  # noqa: E501

    OpenAPI spec version: 3.11.1
    Contact: api@behavioralsignals.com
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


import pprint
import re  # noqa: F401

import six


class ResultJSONFramesSpeakers(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'id': 'int',
        'vad': 'ResultJSONFramesVad',
        'beep': 'ResultJSONFramesBeep',
        'ring': 'ResultJSONFramesRing',
        'strength': 'ResultJSONFramesStrength',
        'snr': 'ResultJSONFramesSnr',
        'positivity': 'ResultJSONFramesPositivity',
        'emotion': 'ResultJSONFramesEmotion',
        'gender': 'ResultJSONFramesGender',
        'language': 'ResultJSONFramesLanguage',
        'age': 'ResultJSONFramesAge',
        'engagement': 'ResultJSONFramesEngagement',
        'activation': 'ResultJSONFramesActivation',
        'politeness': 'ResultJSONFramesPoliteness',
        'success': 'ResultJSONFramesSuccess',
        'speaking_rate': 'ResultJSONFramesSpeakingRate',
        'hesitation': 'ResultJSONFramesHesitation',
        'tone_variety': 'ResultJSONFramesToneVariety',
        'f0': 'ResultJSONFramesF0',
        'escalation': 'int',
        'resolution': 'int',
        'empathy': 'float'
    }

    attribute_map = {
        'id': 'id',
        'vad': 'vad',
        'beep': 'beep',
        'ring': 'ring',
        'strength': 'strength',
        'snr': 'snr',
        'positivity': 'positivity',
        'emotion': 'emotion',
        'gender': 'gender',
        'language': 'language',
        'age': 'age',
        'engagement': 'engagement',
        'activation': 'activation',
        'politeness': 'politeness',
        'success': 'success',
        'speaking_rate': 'speaking_rate',
        'hesitation': 'hesitation',
        'tone_variety': 'tone_variety',
        'f0': 'f0',
        'escalation': 'escalation',
        'resolution': 'resolution',
        'empathy': 'empathy'
    }

    def __init__(self, id=None, vad=None, beep=None, ring=None, strength=None, snr=None, positivity=None, emotion=None, gender=None, language=None, age=None, engagement=None, activation=None, politeness=None, success=None, speaking_rate=None, hesitation=None, tone_variety=None, f0=None, escalation=None, resolution=None, empathy=None):  # noqa: E501
        """ResultJSONFramesSpeakers - a model defined in Swagger"""  # noqa: E501

        self._id = None
        self._vad = None
        self._beep = None
        self._ring = None
        self._strength = None
        self._snr = None
        self._positivity = None
        self._emotion = None
        self._gender = None
        self._language = None
        self._age = None
        self._engagement = None
        self._activation = None
        self._politeness = None
        self._success = None
        self._speaking_rate = None
        self._hesitation = None
        self._tone_variety = None
        self._f0 = None
        self._escalation = None
        self._resolution = None
        self._empathy = None
        self.discriminator = None

        if id is not None:
            self.id = id
        if vad is not None:
            self.vad = vad
        if beep is not None:
            self.beep = beep
        if ring is not None:
            self.ring = ring
        if strength is not None:
            self.strength = strength
        if snr is not None:
            self.snr = snr
        if positivity is not None:
            self.positivity = positivity
        if emotion is not None:
            self.emotion = emotion
        if gender is not None:
            self.gender = gender
        if language is not None:
            self.language = language
        if age is not None:
            self.age = age
        if engagement is not None:
            self.engagement = engagement
        if activation is not None:
            self.activation = activation
        if politeness is not None:
            self.politeness = politeness
        if success is not None:
            self.success = success
        if speaking_rate is not None:
            self.speaking_rate = speaking_rate
        if hesitation is not None:
            self.hesitation = hesitation
        if tone_variety is not None:
            self.tone_variety = tone_variety
        if f0 is not None:
            self.f0 = f0
        if escalation is not None:
            self.escalation = escalation
        if resolution is not None:
            self.resolution = resolution
        if empathy is not None:
            self.empathy = empathy

    @property
    def id(self):
        """Gets the id of this ResultJSONFramesSpeakers.  # noqa: E501

        The speaker index  # noqa: E501

        :return: The id of this ResultJSONFramesSpeakers.  # noqa: E501
        :rtype: int
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this ResultJSONFramesSpeakers.

        The speaker index  # noqa: E501

        :param id: The id of this ResultJSONFramesSpeakers.  # noqa: E501
        :type: int
        """

        self._id = id

    @property
    def vad(self):
        """Gets the vad of this ResultJSONFramesSpeakers.  # noqa: E501


        :return: The vad of this ResultJSONFramesSpeakers.  # noqa: E501
        :rtype: ResultJSONFramesVad
        """
        return self._vad

    @vad.setter
    def vad(self, vad):
        """Sets the vad of this ResultJSONFramesSpeakers.


        :param vad: The vad of this ResultJSONFramesSpeakers.  # noqa: E501
        :type: ResultJSONFramesVad
        """

        self._vad = vad

    @property
    def beep(self):
        """Gets the beep of this ResultJSONFramesSpeakers.  # noqa: E501


        :return: The beep of this ResultJSONFramesSpeakers.  # noqa: E501
        :rtype: ResultJSONFramesBeep
        """
        return self._beep

    @beep.setter
    def beep(self, beep):
        """Sets the beep of this ResultJSONFramesSpeakers.


        :param beep: The beep of this ResultJSONFramesSpeakers.  # noqa: E501
        :type: ResultJSONFramesBeep
        """

        self._beep = beep

    @property
    def ring(self):
        """Gets the ring of this ResultJSONFramesSpeakers.  # noqa: E501


        :return: The ring of this ResultJSONFramesSpeakers.  # noqa: E501
        :rtype: ResultJSONFramesRing
        """
        return self._ring

    @ring.setter
    def ring(self, ring):
        """Sets the ring of this ResultJSONFramesSpeakers.


        :param ring: The ring of this ResultJSONFramesSpeakers.  # noqa: E501
        :type: ResultJSONFramesRing
        """

        self._ring = ring

    @property
    def strength(self):
        """Gets the strength of this ResultJSONFramesSpeakers.  # noqa: E501


        :return: The strength of this ResultJSONFramesSpeakers.  # noqa: E501
        :rtype: ResultJSONFramesStrength
        """
        return self._strength

    @strength.setter
    def strength(self, strength):
        """Sets the strength of this ResultJSONFramesSpeakers.


        :param strength: The strength of this ResultJSONFramesSpeakers.  # noqa: E501
        :type: ResultJSONFramesStrength
        """

        self._strength = strength

    @property
    def snr(self):
        """Gets the snr of this ResultJSONFramesSpeakers.  # noqa: E501


        :return: The snr of this ResultJSONFramesSpeakers.  # noqa: E501
        :rtype: ResultJSONFramesSnr
        """
        return self._snr

    @snr.setter
    def snr(self, snr):
        """Sets the snr of this ResultJSONFramesSpeakers.


        :param snr: The snr of this ResultJSONFramesSpeakers.  # noqa: E501
        :type: ResultJSONFramesSnr
        """

        self._snr = snr

    @property
    def positivity(self):
        """Gets the positivity of this ResultJSONFramesSpeakers.  # noqa: E501


        :return: The positivity of this ResultJSONFramesSpeakers.  # noqa: E501
        :rtype: ResultJSONFramesPositivity
        """
        return self._positivity

    @positivity.setter
    def positivity(self, positivity):
        """Sets the positivity of this ResultJSONFramesSpeakers.


        :param positivity: The positivity of this ResultJSONFramesSpeakers.  # noqa: E501
        :type: ResultJSONFramesPositivity
        """

        self._positivity = positivity

    @property
    def emotion(self):
        """Gets the emotion of this ResultJSONFramesSpeakers.  # noqa: E501


        :return: The emotion of this ResultJSONFramesSpeakers.  # noqa: E501
        :rtype: ResultJSONFramesEmotion
        """
        return self._emotion

    @emotion.setter
    def emotion(self, emotion):
        """Sets the emotion of this ResultJSONFramesSpeakers.


        :param emotion: The emotion of this ResultJSONFramesSpeakers.  # noqa: E501
        :type: ResultJSONFramesEmotion
        """

        self._emotion = emotion

    @property
    def gender(self):
        """Gets the gender of this ResultJSONFramesSpeakers.  # noqa: E501


        :return: The gender of this ResultJSONFramesSpeakers.  # noqa: E501
        :rtype: ResultJSONFramesGender
        """
        return self._gender

    @gender.setter
    def gender(self, gender):
        """Sets the gender of this ResultJSONFramesSpeakers.


        :param gender: The gender of this ResultJSONFramesSpeakers.  # noqa: E501
        :type: ResultJSONFramesGender
        """

        self._gender = gender

    @property
    def language(self):
        """Gets the language of this ResultJSONFramesSpeakers.  # noqa: E501


        :return: The language of this ResultJSONFramesSpeakers.  # noqa: E501
        :rtype: ResultJSONFramesLanguage
        """
        return self._language

    @language.setter
    def language(self, language):
        """Sets the language of this ResultJSONFramesSpeakers.


        :param language: The language of this ResultJSONFramesSpeakers.  # noqa: E501
        :type: ResultJSONFramesLanguage
        """

        self._language = language

    @property
    def age(self):
        """Gets the age of this ResultJSONFramesSpeakers.  # noqa: E501


        :return: The age of this ResultJSONFramesSpeakers.  # noqa: E501
        :rtype: ResultJSONFramesAge
        """
        return self._age

    @age.setter
    def age(self, age):
        """Sets the age of this ResultJSONFramesSpeakers.


        :param age: The age of this ResultJSONFramesSpeakers.  # noqa: E501
        :type: ResultJSONFramesAge
        """

        self._age = age

    @property
    def engagement(self):
        """Gets the engagement of this ResultJSONFramesSpeakers.  # noqa: E501


        :return: The engagement of this ResultJSONFramesSpeakers.  # noqa: E501
        :rtype: ResultJSONFramesEngagement
        """
        return self._engagement

    @engagement.setter
    def engagement(self, engagement):
        """Sets the engagement of this ResultJSONFramesSpeakers.


        :param engagement: The engagement of this ResultJSONFramesSpeakers.  # noqa: E501
        :type: ResultJSONFramesEngagement
        """

        self._engagement = engagement

    @property
    def activation(self):
        """Gets the activation of this ResultJSONFramesSpeakers.  # noqa: E501


        :return: The activation of this ResultJSONFramesSpeakers.  # noqa: E501
        :rtype: ResultJSONFramesActivation
        """
        return self._activation

    @activation.setter
    def activation(self, activation):
        """Sets the activation of this ResultJSONFramesSpeakers.


        :param activation: The activation of this ResultJSONFramesSpeakers.  # noqa: E501
        :type: ResultJSONFramesActivation
        """

        self._activation = activation

    @property
    def politeness(self):
        """Gets the politeness of this ResultJSONFramesSpeakers.  # noqa: E501


        :return: The politeness of this ResultJSONFramesSpeakers.  # noqa: E501
        :rtype: ResultJSONFramesPoliteness
        """
        return self._politeness

    @politeness.setter
    def politeness(self, politeness):
        """Sets the politeness of this ResultJSONFramesSpeakers.


        :param politeness: The politeness of this ResultJSONFramesSpeakers.  # noqa: E501
        :type: ResultJSONFramesPoliteness
        """

        self._politeness = politeness

    @property
    def success(self):
        """Gets the success of this ResultJSONFramesSpeakers.  # noqa: E501


        :return: The success of this ResultJSONFramesSpeakers.  # noqa: E501
        :rtype: ResultJSONFramesSuccess
        """
        return self._success

    @success.setter
    def success(self, success):
        """Sets the success of this ResultJSONFramesSpeakers.


        :param success: The success of this ResultJSONFramesSpeakers.  # noqa: E501
        :type: ResultJSONFramesSuccess
        """

        self._success = success

    @property
    def speaking_rate(self):
        """Gets the speaking_rate of this ResultJSONFramesSpeakers.  # noqa: E501


        :return: The speaking_rate of this ResultJSONFramesSpeakers.  # noqa: E501
        :rtype: ResultJSONFramesSpeakingRate
        """
        return self._speaking_rate

    @speaking_rate.setter
    def speaking_rate(self, speaking_rate):
        """Sets the speaking_rate of this ResultJSONFramesSpeakers.


        :param speaking_rate: The speaking_rate of this ResultJSONFramesSpeakers.  # noqa: E501
        :type: ResultJSONFramesSpeakingRate
        """

        self._speaking_rate = speaking_rate

    @property
    def hesitation(self):
        """Gets the hesitation of this ResultJSONFramesSpeakers.  # noqa: E501


        :return: The hesitation of this ResultJSONFramesSpeakers.  # noqa: E501
        :rtype: ResultJSONFramesHesitation
        """
        return self._hesitation

    @hesitation.setter
    def hesitation(self, hesitation):
        """Sets the hesitation of this ResultJSONFramesSpeakers.


        :param hesitation: The hesitation of this ResultJSONFramesSpeakers.  # noqa: E501
        :type: ResultJSONFramesHesitation
        """

        self._hesitation = hesitation

    @property
    def tone_variety(self):
        """Gets the tone_variety of this ResultJSONFramesSpeakers.  # noqa: E501


        :return: The tone_variety of this ResultJSONFramesSpeakers.  # noqa: E501
        :rtype: ResultJSONFramesToneVariety
        """
        return self._tone_variety

    @tone_variety.setter
    def tone_variety(self, tone_variety):
        """Sets the tone_variety of this ResultJSONFramesSpeakers.


        :param tone_variety: The tone_variety of this ResultJSONFramesSpeakers.  # noqa: E501
        :type: ResultJSONFramesToneVariety
        """

        self._tone_variety = tone_variety

    @property
    def f0(self):
        """Gets the f0 of this ResultJSONFramesSpeakers.  # noqa: E501


        :return: The f0 of this ResultJSONFramesSpeakers.  # noqa: E501
        :rtype: ResultJSONFramesF0
        """
        return self._f0

    @f0.setter
    def f0(self, f0):
        """Sets the f0 of this ResultJSONFramesSpeakers.


        :param f0: The f0 of this ResultJSONFramesSpeakers.  # noqa: E501
        :type: ResultJSONFramesF0
        """

        self._f0 = f0

    @property
    def escalation(self):
        """Gets the escalation of this ResultJSONFramesSpeakers.  # noqa: E501

        Escalation  # noqa: E501

        :return: The escalation of this ResultJSONFramesSpeakers.  # noqa: E501
        :rtype: int
        """
        return self._escalation

    @escalation.setter
    def escalation(self, escalation):
        """Sets the escalation of this ResultJSONFramesSpeakers.

        Escalation  # noqa: E501

        :param escalation: The escalation of this ResultJSONFramesSpeakers.  # noqa: E501
        :type: int
        """

        self._escalation = escalation

    @property
    def resolution(self):
        """Gets the resolution of this ResultJSONFramesSpeakers.  # noqa: E501

        Resolution  # noqa: E501

        :return: The resolution of this ResultJSONFramesSpeakers.  # noqa: E501
        :rtype: int
        """
        return self._resolution

    @resolution.setter
    def resolution(self, resolution):
        """Sets the resolution of this ResultJSONFramesSpeakers.

        Resolution  # noqa: E501

        :param resolution: The resolution of this ResultJSONFramesSpeakers.  # noqa: E501
        :type: int
        """

        self._resolution = resolution

    @property
    def empathy(self):
        """Gets the empathy of this ResultJSONFramesSpeakers.  # noqa: E501

        Speaker empathy score  # noqa: E501

        :return: The empathy of this ResultJSONFramesSpeakers.  # noqa: E501
        :rtype: float
        """
        return self._empathy

    @empathy.setter
    def empathy(self, empathy):
        """Sets the empathy of this ResultJSONFramesSpeakers.

        Speaker empathy score  # noqa: E501

        :param empathy: The empathy of this ResultJSONFramesSpeakers.  # noqa: E501
        :type: float
        """

        self._empathy = empathy

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(ResultJSONFramesSpeakers, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, ResultJSONFramesSpeakers):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
