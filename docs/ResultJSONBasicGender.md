# ResultJSONBasicGender

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**agent** | **int** | Agent gender index: 1&#x3D;female, 2&#x3D;male | [optional] 
**customer** | **int** | Customer gender index: 1&#x3D;female, 2&#x3D;male | [optional] 
**speaker1** | **int** | Speaker gender index: 1&#x3D;female, 2&#x3D;male | [optional] 
**speaker2** | **int** | Speaker gender index: 1&#x3D;female, 2&#x3D;male | [optional] 
**speaker3** | **int** | Speaker gender index: 1&#x3D;female, 2&#x3D;male | [optional] 
**speaker4** | **int** | Speaker gender index: 1&#x3D;female, 2&#x3D;male | [optional] 
**speaker5** | **int** | Speaker gender index: 1&#x3D;female, 2&#x3D;male | [optional] 
**speaker6** | **int** | Speaker gender index: 1&#x3D;female, 2&#x3D;male | [optional] 
**speaker7** | **int** | Speaker gender index: 1&#x3D;female, 2&#x3D;male | [optional] 
**speaker8** | **int** | Speaker gender index: 1&#x3D;female, 2&#x3D;male | [optional] 
**speaker9** | **int** | Speaker gender index: 1&#x3D;female, 2&#x3D;male | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


