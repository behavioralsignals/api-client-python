# coding: utf-8

"""
    Oliver API

    Oliver API service  # noqa: E501

    OpenAPI spec version: 3.11.0
    Contact: api@behavioralsignals.com
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

from __future__ import absolute_import

import unittest

import behavioral_signals_swagger_client_3
from behavioral_signals_swagger_client_3.models.body import Body  # noqa: E501
from behavioral_signals_swagger_client_3.rest import ApiException


class TestBody(unittest.TestCase):
    """Body unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testBody(self):
        """Test Body"""
        # FIXME: construct object with mandatory attributes with example values
        # model = behavioral_signals_swagger_client_3.models.body.Body()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
