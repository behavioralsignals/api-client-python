# SpeakerDiarizationRangesInner

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**st** | **float** | The array containing the segments the speaker was active | [optional] 
**et** | **float** | End time in seconds | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


