# ResultJSONCoreEmotions

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**happy** | [**ResultJSONCoreEmotionsHappy**](ResultJSONCoreEmotionsHappy.md) |  | [optional] 
**anger** | [**ResultJSONCoreEmotionsAnger**](ResultJSONCoreEmotionsAnger.md) |  | [optional] 
**frustration** | [**ResultJSONCoreEmotionsFrustration**](ResultJSONCoreEmotionsFrustration.md) |  | [optional] 
**sad** | [**ResultJSONCoreEmotionsSad**](ResultJSONCoreEmotionsSad.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


