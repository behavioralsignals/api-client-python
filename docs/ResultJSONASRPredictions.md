# ResultJSONASRPredictions

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**task** | **str** | The description of the task for completion | [optional] 
**detections** | [**list[ResultJSONASRDetections]**](ResultJSONASRDetections.md) | Detected areas on ASR result to apply the task | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


